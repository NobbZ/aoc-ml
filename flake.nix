{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.parts.url = "github:hercules-ci/flake-parts";

  outputs = { self, parts , ...} @ inputs: parts.lib.mkFlake { inherit inputs; } {
    systems = [ "x86_64-linux"];

    perSystem = {pkgs, ...}: {
      formatter = pkgs.alejandra;

      devShells.default = pkgs.mkShell {
        packages = builtins.attrValues {
          inherit (pkgs) alejandra nil;
          inherit (pkgs.ocaml-ng.ocamlPackages) ocaml findlib dune_3 ocaml-lsp ocamlformat;
          inherit (pkgs.ocaml-ng.ocamlPackages) core ounit2 utop dune-site;
        }; 
      };
    };
  };
}