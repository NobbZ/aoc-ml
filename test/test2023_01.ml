open OUnit2
open Core
open Aoc_lib.Y2023.D01

let a_test _ctx = assert_equal "56049" (a ()) ~printer:Fn.id
let b_test _ctx = assert_equal "54530" (b ()) ~printer:Fn.id

let tester ~printer exp fut input =
  input >:: fun _ctx -> assert_equal ~printer exp (fut input)

let testerForA exp = tester exp Aoc_lib.Y2023.D01.aWithInput
let testerForB exp = tester exp Aoc_lib.Y2023.D01.bWithInput

(* let testerForB exp = tester exp Aoc_lib.Y2015.D01.bWithInput *)
let mapTest tester exp = List.map (tester exp)
let testerForAValue input value = tester value Aoc_lib.Y2023.D01.calibrate input

let testerForBValue input value =
  tester value Aoc_lib.Y2023.D01.calibrate_b input

let exampleInput = "1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet"

let exampleInputB =
  "two1nine\n\
   eightwothree\n\
   abcone2threexyz\n\
   xtwone3four\n\
   4nineeightseven2\n\
   zoneight234\n\
   7pqrstsixteen"

let tests =
  "d01"
  >::: [
         "a"
         >::: [
                "solution" >:: a_test;
                "values"
                >::: [
                       testerForAValue ~printer:string_of_int "1abc2" 12;
                       testerForAValue ~printer:string_of_int "pqr3stu8vwx" 38;
                       testerForAValue ~printer:string_of_int "a1b2c3d4e5f" 15;
                       testerForAValue ~printer:string_of_int "treb7uchet" 77;
                     ];
                "example" >::: [ testerForA ~printer:Fn.id "142" exampleInput ];
              ];
         "b"
         >::: [
                "solution" >:: b_test;
                "values"
                >::: [
                       testerForBValue ~printer:string_of_int "two1nine" 29;
                       testerForBValue ~printer:string_of_int "eightwothree" 83;
                       testerForBValue ~printer:string_of_int "abcone2threexyz"
                         13;
                       testerForBValue ~printer:string_of_int "xtwone3four" 24;
                       testerForBValue ~printer:string_of_int "4nineeightseven2"
                         42;
                       testerForBValue ~printer:string_of_int "zoneight234" 14;
                       testerForBValue ~printer:string_of_int "7pqrstsixteen" 76;
                     ];
                "example" >::: [testerForB ~printer:Fn.id "281" exampleInputB];
              ];
       ]
