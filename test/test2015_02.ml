open OUnit2
open Aoc_lib.Y2015.D02

let a_test _ctx = assert_equal "1606483" (a ())
let b_test _ctx = assert_equal "3842356" (b ())

let wrap_test (l, w, h, exp) =
  let label = Printf.sprintf "%dx%dx%d" l w h in
  label >:: fun _ctx -> assert_equal exp (calcWrap l w h)

let wrap = [ (2, 3, 4, 58); (1, 1, 10, 43) ] |> List.map wrap_test

let base_test (l, w, h, exp) =
  let label = Printf.sprintf "%dx%dx%d" l w h in
  label >:: fun _ctx -> assert_equal exp (calcBaseRibbon l w h)

let base = [ (2, 3, 4, 10); (1, 1, 10, 4) ] |> List.map base_test

let bow_test (l, w, h, exp) =
  let label = Printf.sprintf "%dx%dx%d" l w h in
  label >:: fun _ctx -> assert_equal exp (calcBowRibbon l w h)

let bow = [ (2, 3, 4, 24); (1, 1, 10, 10) ] |> List.map bow_test

let tests =
  "d02"
  >::: [
         "a" >::: [ "solution" >:: a_test; "examples" >::: wrap ];
         "b"
         >::: [
                "solution" >:: b_test;
                "base" >::: base;
                "bow" >::: bow;
                "sum" >::: [];
              ];
       ]
