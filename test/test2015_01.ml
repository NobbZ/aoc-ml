open OUnit2

let tester exp fut input = input >:: fun _ctx -> assert_equal exp (fut input)
let testerForA exp = tester exp Aoc_lib.Y2015.D01.aWithInput
let testerForB exp = tester exp Aoc_lib.Y2015.D01.bWithInput
let mapTest tester exp = List.map (tester exp)
let floor0 = [ "(())"; "()()" ] |> mapTest testerForA "0"
let floor3 = [ "((("; "(()(()("; "))(((((" ] |> mapTest testerForA "3"
let floorM1 = [ "())"; "))(" ] |> mapTest testerForA "-1"
let floorM3 = [ ")))"; ")())())" ] |> mapTest testerForA "-3"
let aTest = [ testerForA "232" (Aoc_lib.Y2015.D01.input ()) ]
let bTest = [ testerForB "1783" (Aoc_lib.Y2015.D01.input ()) ]

let tests =
  "d01"
  >::: [
         "a"
         >::: [
                "solution" >::: aTest;
                "floor0" >::: floor0;
                "floor3" >::: floor3;
                "floor -1" >::: floorM1;
                "floor -3" >::: floorM3;
              ];
         "b"
         >::: [
                "solution" >::: bTest;
                "steps1" >::: [ testerForB "1" ")" ];
                "steps5" >::: [ testerForB "5" "()())" ];
              ];
       ]
