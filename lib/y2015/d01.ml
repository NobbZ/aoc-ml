open Core
open Aoc_support

let year = 2015
let day = 1

exception No_paren
exception Bad_floor

let aWithInput input =
  let f acc = function
    | '(' -> acc + 1
    | ')' -> acc - 1
    | _ -> raise No_paren
  in
  input |> String.fold ~init:0 ~f |> Int.to_string

let bWithInput input =
  let f acc p =
    let open Continue_or_stop in
    match (acc, p) with
    | (-1, steps), _ -> Stop steps
    | (fl, steps), '(' -> Continue (fl + 1, steps + 1)
    | (fl, steps), ')' -> Continue (fl - 1, steps + 1)
    | _, _ -> raise No_paren
  in
  let finish = function
    | fl, steps -> if Int.( <> ) fl (-1) then raise Bad_floor else steps
  in
  input |> String.fold_until ~init:(0, 0) ~f ~finish |> Int.to_string

let input () = Inputs.get_input_exn year day
let a () = input () |> aWithInput
let b () = input () |> bWithInput
