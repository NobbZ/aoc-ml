open Core
open Aoc_support

let year = 2015
let day = 2

exception Unimplemented of string
exception Unreachable of string

let parse input =
  input |> String.split ~on:'\n'
  |> List.map ~f:(String.split ~on:'x')
  |> List.map ~f:(List.map ~f:Int.of_string)
  |> List.map ~f:(function
       | [ l; w; h ] -> (l, w, h)
       | _ -> raise (Unreachable "parse"))

let calcWrap l w h =
  let a = l * w and b = l * h and c = w * h in
  let smallest =
    [ a; b; c ] |> List.min_elt ~compare:( - ) |> Option.value_exn
  in
  (2 * a) + (2 * b) + (2 * c) + smallest

let calcBaseRibbon l w h =
  match [ l; w; h ] |> List.sort ~compare:( - ) with
  | a :: b :: _ -> (2 * a) + (2 * b)
  | _ -> raise (Unreachable "calcBaseRibbon")

let calcBowRibbon l w h = l * w * h

let aWithInput input =
  input |> parse
  |> List.sum ~f:(function l, w, h -> calcWrap l w h) (module Int)
  |> Int.to_string

let bWithInput input =
  input |> parse
  |> List.sum
       ~f:(function l, w, h -> calcBaseRibbon l w h + calcBowRibbon l w h)
       (module Int)
  |> Int.to_string

let input () = Inputs.get_input_exn year day
let a () = input () |> aWithInput
let b () = input () |> bWithInput
