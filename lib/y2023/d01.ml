open Core
open Aoc_support

let year = 2023
let day = 1

let findFirstDigit chars =
  let rec findFirstDigit' = function
    | [] -> failwith "findFirstDigit' []: unreachable!"
    | c :: cs -> if Char.is_digit c then c else findFirstDigit' cs
  in
  findFirstDigit' chars

let calibrate line =
  let chars = line |> String.to_list in
  let firstDigit = chars |> findFirstDigit |> Char.get_digit_exn in
  let lastDigit = chars |> List.rev |> findFirstDigit |> Char.get_digit_exn in
  (firstDigit * 10) + lastDigit

let rec findForwardDigit = function
  | '1' :: _ -> 1
  | '2' :: _ -> 2
  | '3' :: _ -> 3
  | '4' :: _ -> 4
  | '5' :: _ -> 5
  | '6' :: _ -> 6
  | '7' :: _ -> 7
  | '8' :: _ -> 8
  | '9' :: _ -> 9
  | '0' :: _ -> 0
  | 'o' :: 'n' :: 'e' :: _ -> 1
  | 't' :: 'w' :: 'o' :: _ -> 2
  | 't' :: 'h' :: 'r' :: 'e' :: 'e' :: _ -> 3
  | 'f' :: 'o' :: 'u' :: 'r' :: _ -> 4
  | 'f' :: 'i' :: 'v' :: 'e' :: _ -> 5
  | 's' :: 'i' :: 'x' :: _ -> 6
  | 's' :: 'e' :: 'v' :: 'e' :: 'n' :: _ -> 7
  | 'e' :: 'i' :: 'g' :: 'h' :: 't' :: _ -> 8
  | 'n' :: 'i' :: 'n' :: 'e' :: _ -> 9
  | _ :: s -> findForwardDigit s
  | [] -> failwith "findForwardDigit: unreachable (invariant of input data)"

let rec findReverseDigit s =
  match s with
  | '1' :: _ -> 1
  | '2' :: _ -> 2
  | '3' :: _ -> 3
  | '4' :: _ -> 4
  | '5' :: _ -> 5
  | '6' :: _ -> 6
  | '7' :: _ -> 7
  | '8' :: _ -> 8
  | '9' :: _ -> 9
  | '0' :: _ -> 0
  | 'e' :: 'n' :: 'o' :: _ -> 1
  | 'o' :: 'w' :: 't' :: _ -> 2
  | 'e' :: 'e' :: 'r' :: 'h' :: 't' :: _ -> 3
  | 'r' :: 'u' :: 'o' :: 'f' :: _ -> 4
  | 'e' :: 'v' :: 'i' :: 'f' :: _ -> 5
  | 'x' :: 'i' :: 's' :: _ -> 6
  | 'n' :: 'e' :: 'v' :: 'e' :: 's' :: _ -> 7
  | 't' :: 'h' :: 'g' :: 'i' :: 'e' :: _ -> 8
  | 'e' :: 'n' :: 'i' :: 'n' :: _ -> 9
  | _ :: s -> findReverseDigit s
  | [] -> failwith "findReverseDigit: unreachable (invariant of input data)"

let calibrate_b line =
  let chars = String.to_list line in
  let firstDigit = chars |> findForwardDigit in
  let lastDigit = chars |> List.rev |> findReverseDigit in
  (firstDigit * 10) + lastDigit

let aWithInput input =
  input |> String.split_lines |> List.map ~f:calibrate
  |> List.sum (module Int) ~f:Fn.id
  |> Int.to_string

let bWithInput input =
  input |> String.split_lines |> List.map ~f:calibrate_b
  |> List.sum (module Int) ~f:Fn.id
  |> Int.to_string

let input () = Inputs.get_input_exn year day
let a () = input () |> aWithInput
let b () = input () |> bWithInput
