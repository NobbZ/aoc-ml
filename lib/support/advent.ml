module type T = sig
  val year : int
  val day : int
  val a : unit -> string
  val aWithInput : string -> string
  val b : unit -> string
  val bWithInput : string -> string
end
