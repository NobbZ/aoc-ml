let locations = Aoc_inputs.Sites.data

exception No_file of (int * int)

let lookup_file dirs year day =
  List.find_map
    (fun dir ->
      let path =
        Filename.concat dir (Printf.sprintf "y%04d_d%02d.txt" year day)
      in

      if Sys.file_exists path then Some path else None)
    dirs

let get_input_exn year day =
  match lookup_file locations year day with
  | Some path -> path |> Core.In_channel.read_all |> Core.String.strip
  | None -> raise (No_file (year, day))
