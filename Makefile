ML_SRCS = $(shell find bin lib test -name '*.ml')
INPUTS = $(shell find data -name '*.txt')

run:
	dune exec --release aoc

test: _build/default/bin/aoc.exe
	dune runtest --force
.PHONY: test

build: aoc

format:
	ocamlformat --inplace ${ML_SRCS}

clean:
	dune clean
	rm -f aoc

aoc: _build/default/bin/aoc.exe
	install --mode=755 $< $@

_build/default/bin/aoc.exe: ${ML_SRCS} ${INPUTS}
	dune build --release
