{ pkgs ? import (builtins.fetchTarball "https://github.com/NixOS/nixpkgs-channels/archive/dc80d7bc4a244120b3d766746c41c0d9c5f81dfa.tar.gz") {
    config.allowUnfree = true;
  }
}:
let
  inherit (pkgs.ocaml-ng.ocamlPackages_4_10) buildDunePackage findlib dune_2
    ocaml core uuseg re odoc ocaml-migrate-parsetree stdio fpath cmdliner menhir uutf fix
    ounit2 utop;
  inherit (pkgs) gnum4 opam vscode-with-extensions vscode-utils ocaml-lsp
    fetchFromGitHub;

  vscode = vscode-with-extensions.override {
    vscodeExtensions = vscode-utils.extensionsFromVscodeMarketplace [{
      name = "ocaml-platform";
      publisher = "ocamllabs";
      version = "0.8.0";
      sha256 = "0862zy262cb37y49xjfqmjzqx23425189sm0qfmyf2myn2pa7yhm";
    }];
  };

  ocaml-format = buildDunePackage rec {
    pname = "ocamlformat";
    version = "0.14.2";

    useDune2 = true;

    buildInputs = [ uuseg re odoc ocaml-migrate-parsetree stdio fpath cmdliner menhir uutf fix ];

    src = fetchFromGitHub {
      owner = "ocaml-ppx";
      repo = "ocamlformat";
      rev = version;
      sha256 = "03n3d3l83mw4cfdz8w87hn66pzyvklgq1gwdwi8kndsmslwfcp4y";
    };
  };
in
pkgs.mkShell {
  buildInputs = [ vscode findlib dune_2 gnum4 ocaml core ocaml-lsp ocaml-format ounit2 opam ocaml-lsp utop ];
}
