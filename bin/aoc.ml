open Core

module type Advent = Aoc_support.Advent.T

let measure (f : unit -> 'a) : Time_ns.Span.t * 'a =
  let start = Time_ns.now () in
  let result = f () in
  let stop = Time_ns.now () in
  let dur = Time_ns.diff stop start in
  (dur, result)

let doDay m =
  let module M = (val m : Advent) in
  let y = M.year in
  let d = M.day in
  let da, a = measure M.a in
  let db, b = measure M.b in
  let open Printf in
  printf "%4d:%02d:a %10s  %10s\n" y d a (Time_ns.Span.to_string da);
  printf "    :  :b %10s  %10s\n" b (Time_ns.Span.to_string db)

let () =
  let y15 : (module Advent) list =
    [ (module Aoc_lib.Y2015.D01); (module Aoc_lib.Y2015.D02) ]
  in
  let y23 : (module Advent) list = [ (module Aoc_lib.Y2023.D01) ] in
  let all = y15 @ y23 in
  List.iter all ~f:doDay
